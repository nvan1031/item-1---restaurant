package com.rhx.Common;

import com.rhx.Exception.CoustomException;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSessionException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLIntegrityConstraintViolationException;

/**
 * @author 任恒绪
 * @version 1.0
 */
@ControllerAdvice(annotations = {RestController.class})
@ResponseBody
@Slf4j
public class GlobalInterceptor {

    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public R SqlInterceptor(SQLIntegrityConstraintViolationException exception){
       String msg = exception.getMessage();
       log.info(msg);
       if (msg.contains("Duplicate entry")){
           String name = msg.split(" ")[2];
           return R.error(name+"已存在");

       }
       return  R.error("未知错误");



    }
    @ExceptionHandler(CoustomException.class)
    public R CustomInterceptor(CoustomException ex){
        String msg = ex.getMessage();
        log.info(msg);
        return R.error(msg);

    }


}
