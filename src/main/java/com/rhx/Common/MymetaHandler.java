package com.rhx.Common;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.prefs.BackingStoreException;

/**
 * @author 任恒绪
 * @version 1.0
 */
@Component
@Slf4j
public class MymetaHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("公共字段填充[增加]");
        metaObject.setValue("createTime", LocalDateTime.now());
        metaObject.setValue("updateTime",LocalDateTime.now());
        metaObject.setValue("createUser", BaseContent.getId());

        metaObject.setValue("updateUser",BaseContent.getId());


    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("公共字段填充[更新]");

        metaObject.setValue("updateTime",LocalDateTime.now());
        metaObject.setValue("updateUser",BaseContent.getId());
    }

}
