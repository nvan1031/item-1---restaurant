package com.rhx.Common;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 任恒绪
 * @version 1.0
 * mybatisplus分页器
 */
@Configuration
@Slf4j
public class MybatisInterceptor {

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor(){
        log.info("MP分页器");
        MybatisPlusInterceptor mr = new MybatisPlusInterceptor();
        mr.addInnerInterceptor(new PaginationInnerInterceptor());
          return  mr;

    }

}
