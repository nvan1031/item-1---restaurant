package com.rhx.Common;

import lombok.extern.slf4j.Slf4j;

/**
 * @author 任恒绪
 * @version 1.0
 */
@Slf4j
public class BaseContent {
    /*
    *
    * 建立当前ThreadLocal
    *
    * */
    private static ThreadLocal<Long> threadLocal = new ThreadLocal();
    /*
    *
    *存入id
    *
    * */
    public static void  setId(Long id){
        log.info("存储当前ID: {}",id);
       threadLocal.set(id);
    }
    /*
    *
    * 获取 ID
    *
    * */
     public static Long  getId(){
         log.info("获取当前ID: {}",threadLocal.get());
         return threadLocal.get();

     }



}
