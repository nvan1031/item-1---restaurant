package com.rhx.Exception;

/**
 * @author 任恒绪
 * @version 1.0
 */
public class CoustomException extends RuntimeException{
    public CoustomException(String message) {
        super(message);
    }
}
