package com.rhx.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rhx.domain.SetmealDish;

/**
 * @author 任恒绪
 * @version 1.0
 */
public interface SetmealDishService extends IService<SetmealDish> {
}
