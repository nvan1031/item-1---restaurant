package com.rhx.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rhx.domain.ShoppingCart;

/**
 * @author 任恒绪
 * @version 1.0
 */
public interface ShoppingCartService extends IService<ShoppingCart> {
}
