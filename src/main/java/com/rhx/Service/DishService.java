package com.rhx.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rhx.domain.Dish;
import com.rhx.domain.Dto.DishDto;

import java.util.List;

/**
 * @author 任恒绪
 * @version 1.0
 */
public interface DishService extends IService<Dish> {

  //保存菜品及其口味信息
  public  void  savewithFlavors(DishDto dishDto);
//  获取菜品及其口味信息
  public  DishDto getdishwithFlavors(Long id);
//  修改菜品及其口味信息。
  public  void updatadishwithFlavors(DishDto dishDto);
//批量更新
  public  void updatestatusByids(int status, List<Long> ids);

}
