package com.rhx.Service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rhx.Mapper.UserMapper;
import com.rhx.Service.UserService;
import com.rhx.domain.User;
import org.springframework.stereotype.Service;

/**
 * @author 任恒绪
 * @version 1.0
 */
@Service
public class Userserviceimpl extends ServiceImpl<UserMapper, User> implements UserService {
}
