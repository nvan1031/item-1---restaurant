package com.rhx.Service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rhx.Mapper.DishMapper;
import com.rhx.Service.DishFlavorsService;
import com.rhx.Service.DishService;
import com.rhx.domain.Dish;
import com.rhx.domain.Dto.DishDto;
import com.rhx.domain.DishFlavor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 任恒绪
 * @version 1.0
 */
@Service
public class DishServiceimpl extends ServiceImpl<DishMapper, Dish> implements DishService {

    @Autowired
    private DishFlavorsService dishFlavorsService;

//增加菜品及其口味信息
    @Override
    @Transactional
    public void savewithFlavors(DishDto dishDto) {
        this.save(dishDto);

        Long disId  = dishDto.getId();
        List<DishFlavor> flavors = dishDto.getFlavors();
        //增加ID输入DISHFlavors
        flavors = flavors.stream().map((item)->{

                    item.setDishId(disId);
                    return item;
                }
        ).collect(Collectors.toList());

        dishFlavorsService.saveBatch(flavors);

    }
/*
*
* 获取可以查询带口味的Dish
* */
    @Override
    public DishDto getdishwithFlavors(Long id) {
       Dish dish =  this.getById(id);

       DishDto dishDto = new DishDto();
//       复制dish的信息到dishdto
       BeanUtils.copyProperties(dish,dishDto);

        LambdaQueryWrapper<DishFlavor> lr = new LambdaQueryWrapper();
       lr.eq(DishFlavor::getDishId,id);
        List<DishFlavor> flavors = dishFlavorsService.list(lr);
        dishDto.setFlavors(flavors);

        return  dishDto;
    }
   @Transactional
    @Override
    public void updatadishwithFlavors(DishDto dishDto) {
//        修改菜品信息
              this.updateById(dishDto);

//        首先删除口味表中的口味信息
        LambdaQueryWrapper<DishFlavor> lr = new LambdaQueryWrapper();
        lr.eq(DishFlavor::getDishId,dishDto.getId());

        dishFlavorsService.remove(lr);

//        增加新的口味信息
        List<DishFlavor> flavors = dishDto.getFlavors();
        flavors = flavors.stream().map(item->{

            item.setDishId(dishDto.getId());
            return item;
        }).collect(Collectors.toList());
       dishFlavorsService.saveBatch(flavors);

    }

    @Override
    public void updatestatusByids(int status, List<Long> ids) {
        for (Long i : ids) {
//          根据id查询当前菜品
         Dish dish =    this.getById(i);
//      设置为新状态
         dish.setStatus(status);
//         更新
            this.updateById(dish);

        }


    }


}
