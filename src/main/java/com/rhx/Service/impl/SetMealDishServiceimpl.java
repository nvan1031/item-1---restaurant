package com.rhx.Service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rhx.Mapper.SetmealDishMapper;
import com.rhx.Service.SetmealDishService;
import com.rhx.domain.SetmealDish;
import org.springframework.stereotype.Service;

/**
 * @author 任恒绪
 * @version 1.0
 */
@Service
public class SetMealDishServiceimpl  extends ServiceImpl<SetmealDishMapper, SetmealDish> implements SetmealDishService {
}
