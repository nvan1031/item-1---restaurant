package com.rhx.Service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rhx.Mapper.AddressBookMapper;
import com.rhx.Service.AddressBookService;
import com.rhx.domain.AddressBook;
import org.springframework.stereotype.Service;

/**
 * @author 任恒绪
 * @version 1.0
 */
@Service
public class AddressBookServiceimpl extends ServiceImpl<AddressBookMapper, AddressBook> implements AddressBookService {
}
