package com.rhx.Service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rhx.Exception.CoustomException;
import com.rhx.Mapper.MealMapper;
import com.rhx.Service.SetmealDishService;
import com.rhx.Service.SetmealService;
import com.rhx.domain.Dish;
import com.rhx.domain.Dto.SetmealDto;
import com.rhx.domain.Setmeal;
import com.rhx.domain.SetmealDish;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


/**
 * @author 任恒绪
 * @version 1.0
 */
@Service
public class SetmealServiceimpl extends ServiceImpl<MealMapper, Setmeal>implements SetmealService {

   @Autowired
   private SetmealDishService setmealDishService;
   @Transactional
   @Override
    public void saveMealDish(SetmealDto setmealDto) {
        //将套餐信息保存到套餐表
        this.save(setmealDto);
//         保存菜品和套餐关系到关系表

        List<SetmealDish>  setmealDishes = setmealDto.getSetmealDishes();
            setmealDishes.stream().map(item->{
                item.setSetmealId(setmealDto.getId());
                return item;
            }).collect(Collectors.toList());
            setmealDishService.saveBatch(setmealDishes);
    }

    @Override
    @Transactional
    public void deleteMealDish(List<Long> ids) {
    //查询是否处于停售状态
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.in(Setmeal::getId,ids);
        queryWrapper.eq(Setmeal::getStatus,1);
        int count = this.count(queryWrapper);
//存在处于售卖状态的套餐抛出业务异常
        if (count > 0)
            throw new CoustomException("套餐处于售卖状态，无法删除");
//     没有处于售卖状态，则首先删除套餐信息
        this.removeByIds(ids);
//        再删除关系表中的信息
        LambdaQueryWrapper <SetmealDish> lambdaQueryWrapper  = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(SetmealDish::getSetmealId,ids);

        setmealDishService.remove(lambdaQueryWrapper);

    }


}
