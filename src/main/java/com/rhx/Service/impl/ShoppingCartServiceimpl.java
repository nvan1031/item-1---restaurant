package com.rhx.Service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rhx.Mapper.ShoppingCartMapper;
import com.rhx.Service.ShoppingCartService;
import com.rhx.domain.ShoppingCart;
import org.springframework.stereotype.Service;

/**
 * @author 任恒绪
 * @version 1.0
 */
@Service
public class ShoppingCartServiceimpl extends ServiceImpl<ShoppingCartMapper, ShoppingCart> implements ShoppingCartService {
}
