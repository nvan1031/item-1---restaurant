package com.rhx.Service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rhx.Common.BaseContent;
import com.rhx.Exception.CoustomException;
import com.rhx.Mapper.OrderDetailMapper;
import com.rhx.Mapper.OrderMapper;
import com.rhx.Service.*;
import com.rhx.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author 任恒绪
 * @version 1.0
 */
@Service
public class OrderServiceimpl extends ServiceImpl<OrderMapper, Orders> implements OrderService {


    @Autowired
    private OrderDetailService orderDetailService;
    @Autowired
    private AddressBookService addressBookService;
    @Autowired
    private ShoppingCartService shoppingCartService;
    @Autowired
    private UserService userService;


/*
* 提交用户订单
* */
   @Transactional
    @Override
    public void submit(Orders orders) {
//获取当前用户ID
        Long userId = BaseContent.getId();

//        查询当前用户购物车数据，并且查询地址相关信息
        LambdaQueryWrapper<ShoppingCart> queryWrapper  = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId,userId);
        //查询购物车
       List<ShoppingCart> shoppingCart = shoppingCartService.list(queryWrapper);
       //判断购物车是否为空
        if (shoppingCart == null){
            throw new CoustomException("购物车为空");
        }
//       获取传入的地址ID
        Long addressid = orders.getAddressBookId();
      AddressBook   addressBook = addressBookService.getById(addressid);
        if(addressBook == null){
            throw new CoustomException("用户地址信息有误，不能下单");
        }

    //查询用户信息
        User user = userService.getById(userId);
//生成订单号ID
        long orderId = IdWorker.getId();
        //填充order
        AtomicInteger amount = new AtomicInteger(0);
        orders.setId(orderId);
        orders.setOrderTime(LocalDateTime.now());
        orders.setCheckoutTime(LocalDateTime.now());
        orders.setStatus(2);
        orders.setAmount(new BigDecimal(amount.get()));//总金额
        orders.setUserId(userId);
        orders.setNumber(String.valueOf(orderId));
        orders.setUserName(user.getName());
        orders.setConsignee(addressBook.getConsignee());
        orders.setPhone(addressBook.getPhone());
        orders.setAddress((addressBook.getProvinceName() == null ? "" : addressBook.getProvinceName())
                + (addressBook.getCityName() == null ? "" : addressBook.getCityName())
                + (addressBook.getDistrictName() == null ? "" : addressBook.getDistrictName())
                + (addressBook.getDetail() == null ? "" : addressBook.getDetail()));

//填充Orderdetails
        List<OrderDetail> orderDetails = shoppingCart.stream().map((item) -> {
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setOrderId(orderId);
            orderDetail.setNumber(item.getNumber());
            orderDetail.setDishFlavor(item.getDishFlavor());
            orderDetail.setDishId(item.getDishId());
            orderDetail.setSetmealId(item.getSetmealId());
            orderDetail.setName(item.getName());
            orderDetail.setImage(item.getImage());
            orderDetail.setAmount(item.getAmount());
            amount.addAndGet(item.getAmount().multiply(new BigDecimal(item.getNumber())).intValue());
            return orderDetail;
        }).collect(Collectors.toList());
//存储order到order表
        this.save(orders);
//        存储orderdetalis到表
        orderDetailService.saveBatch(orderDetails);

        //清空购物车
       LambdaQueryWrapper<ShoppingCart> lambdaQueryWrapper = new LambdaQueryWrapper<>();
       lambdaQueryWrapper.eq(ShoppingCart::getUserId,userId);

       shoppingCart.remove(lambdaQueryWrapper);
       

    }
}
