package com.rhx.Service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rhx.Common.R;
import com.rhx.Exception.CoustomException;
import com.rhx.Mapper.CategoryMapper;
import com.rhx.Service.CategoryService;
import com.rhx.Service.DishService;

import com.rhx.Service.SetmealService;
import com.rhx.domain.Category;
import com.rhx.domain.Dish;
import com.rhx.domain.Setmeal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author 任恒绪
 * @version 1.0
 */
@Service
@Slf4j
public class CategoryServiceimpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {

    @Autowired
  private   DishService dishService;
   @Autowired
  private SetmealService setmealSercvice;
    @Override
    public void remove(Long id) {

        System.out.println(id);
        // 查询当前分类是否关联了菜品,关联则抛出异常
        LambdaQueryWrapper<Dish> lr1 = new LambdaQueryWrapper();
        lr1.eq(Dish::getCategoryId,id);
        int count1  = dishService.count(lr1);
        if (count1 > 0){
            log.info("此分类已关联了套餐，存在 {} 条数据",count1);
            throw (new CoustomException("此分类已关联了菜品"));

        }


        //查询当前分类是否关联了套餐，关联则抛出异常
        LambdaQueryWrapper<Setmeal> lr2 = new LambdaQueryWrapper();
        lr2.eq(Setmeal::getCategoryId,id);
        int count2  = setmealSercvice.count(lr2);
        if (count2 > 0){
            log.info("此分类已关联了套餐，存在 {} 条数据",count2);
            throw (new CoustomException("此分类已关联了套餐"));
        }
        //都未关联进行删除操作
        super.removeById(id);

    }




}
