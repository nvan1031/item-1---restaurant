package com.rhx.Service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rhx.Mapper.DishFlavorsMapper;
import com.rhx.Mapper.DishMapper;
import com.rhx.Service.DishFlavorsService;
import com.rhx.Service.DishService;
import com.rhx.domain.DishFlavor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 任恒绪
 * @version 1.0
 */

@Service
public class DishFlavorsServiceimpl extends ServiceImpl<DishFlavorsMapper, DishFlavor> implements DishFlavorsService {
    @Override
    public void adds(List<DishFlavor> flavor) {
        for (DishFlavor dr : flavor) {
            this.save(dr);
        }

    }
}
