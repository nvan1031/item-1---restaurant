package com.rhx.Service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rhx.Mapper.EmployeeeMapper;
import com.rhx.Service.EmployeeService;
import com.rhx.domain.Employee;
import org.springframework.stereotype.Service;

/**
 * @author 任恒绪
 * @version 1.0
 */
@Service
public class EmployeeServiceimpl extends ServiceImpl<EmployeeeMapper, Employee> implements EmployeeService {
}
