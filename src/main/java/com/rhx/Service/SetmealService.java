package com.rhx.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rhx.domain.Dto.SetmealDto;
import com.rhx.domain.Setmeal;

import java.util.List;

/**
 * @author 任恒绪
 * @version 1.0
 */
public interface SetmealService extends IService<Setmeal> {
//   保存套餐信息和关系表
    public void saveMealDish(SetmealDto setmealDto);
//    删除套餐并删除关系表
    public void deleteMealDish(List<Long> ids);

}
