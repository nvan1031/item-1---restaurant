package com.rhx.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rhx.domain.AddressBook;

/**
 * @author 任恒绪
 * @version 1.0
 */
public interface AddressBookService extends IService<AddressBook> {
}
