package com.rhx.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rhx.domain.Category;

/**
 * @author 任恒绪
 * @version 1.0
 */
public interface CategoryService extends IService<Category> {
    /*
     * 自定义删除方法，增加删除条件判断。
     * */
    public void remove(Long id);
}
