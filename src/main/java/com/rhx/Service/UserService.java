package com.rhx.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rhx.domain.User;

/**
 * @author 任恒绪
 * @version 1.0
 */

public interface UserService extends IService<User> {
}
