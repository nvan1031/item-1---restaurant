package com.rhx.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rhx.domain.DishFlavor;

import java.util.List;

/**
 * @author 任恒绪
 * @version 1.0
 */
public interface DishFlavorsService extends IService<DishFlavor> {
    public void adds(List<DishFlavor> flavor);
}
