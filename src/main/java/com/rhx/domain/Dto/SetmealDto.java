package com.rhx.domain.Dto;


import com.rhx.domain.Setmeal;
import com.rhx.domain.SetmealDish;
import lombok.Data;
import java.util.List;

@Data
public class SetmealDto extends Setmeal {

    private List<SetmealDish> setmealDishes;

    private String categoryName;


}
