package com.rhx.Config;

import com.rhx.Common.JacksonObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.List;

/**
 * @author 任恒绪
 * @version 1.0
 */
@Slf4j
@Configuration
public class SpringMvcConfig extends WebMvcConfigurationSupport {

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        log.info("静态资源加载.....");
      registry.addResourceHandler("/backend/**").addResourceLocations("classpath:/backend/");
      registry.addResourceHandler("/front/**").addResourceLocations("classpath:/front/");


    }

    @Override
    protected void extendMessageConverters(List<HttpMessageConverter<?>> converters) {

        log.info("消息转换器");
        //创建消息转换器
        MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
         //设置消息转换器为我们写的转换器

          mappingJackson2HttpMessageConverter.setObjectMapper(new JacksonObjectMapper());
         //将转换器加入MVC框架

          converters.add(0,mappingJackson2HttpMessageConverter);

    }
}
