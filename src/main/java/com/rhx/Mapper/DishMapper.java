package com.rhx.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rhx.domain.Dish;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.jmx.export.annotation.ManagedOperationParameter;

/**
 * @author 任恒绪
 * @version 1.0
 */
@Mapper
public interface DishMapper extends BaseMapper<Dish> {



}
