package com.rhx.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rhx.domain.SetmealDish;
import org.apache.ibatis.annotations.Mapper;


/**
 * @author 任恒绪
 * @version 1.0
 */
@Mapper
public interface SetmealDishMapper extends BaseMapper<SetmealDish> {
}
