package com.rhx.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rhx.domain.Orders;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 任恒绪
 * @version 1.0
 */
@Mapper
public interface OrderMapper extends BaseMapper<Orders> {
}
