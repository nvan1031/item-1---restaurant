package com.rhx.Filter;

import com.alibaba.fastjson.JSON;
import com.rhx.Common.BaseContent;
import com.rhx.Common.R;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.logging.stdout.StdOutImpl;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

/**
 * @author 任恒绪
 * @version 1.0
 */
@WebFilter(filterName = "PandaFileter",urlPatterns = "/*")
@Slf4j
public class PandaFilter implements Filter {
  private static final AntPathMatcher PATH_MATH = new AntPathMatcher();

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request  = (HttpServletRequest) servletRequest;
        String url = request.getRequestURI();
        log.info("拦截到:"+url);
        String[] urls = {
                "/backend/**",
                "/employee/login",
                "/employee/logout",
                "/front/**",
                "/common/**",
                "/user/sendMsg",
                "/user/login"

        };
        if (check(urls,url)) {
            log.info("本次请求{"+url+"}不需要处理");
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        else {
            if (request.getSession().getAttribute("employee") != null) {

                log.info("用户已登录,用户ID为："+request.getSession().getAttribute("employee"));
                BaseContent.setId((Long)((HttpServletRequest) servletRequest).getSession().getAttribute("employee"));
                filterChain.doFilter(servletRequest, servletResponse);
                return;
            }
            else if (request.getSession().getAttribute("user")!=null){

                log.info("用户已登录,用户ID为："+request.getSession().getAttribute("user"));
                BaseContent.setId((Long)((HttpServletRequest) servletRequest).getSession().getAttribute("user"));
                filterChain.doFilter(servletRequest, servletResponse);
                return;



            }
            else{
                log.info("用户未登录");
                servletResponse.getWriter().write(JSON.toJSONString(R.error("NOTLOGIN")));
                return;
            }
        }
    }
    public boolean check(String []urls,String url){
        for (String  s : urls) {
            if (PATH_MATH.match(s,url))
               return true;
        }
        return false;
    }



}
