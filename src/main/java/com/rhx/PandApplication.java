package com.rhx;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author 任恒绪
 * @version 1.0
 */
@Slf4j
@SpringBootApplication
@ServletComponentScan(basePackages = "com.rhx.Filter")
@EnableTransactionManagement
public class PandApplication {
    public static void main(String[] args) {

        SpringApplication.run(PandApplication.class,args);

        log.info("Springboot程序启动成功....");
    }

}
