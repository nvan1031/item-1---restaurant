package com.rhx.Controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rhx.Common.R;
import com.rhx.Service.CategoryService;
import com.rhx.domain.Category;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.pattern.PathPattern;

import java.util.List;

/**
 * @author 任恒绪
 * @version 1.0
 */

@RestController
@RequestMapping("/category")
@Slf4j
public class CategoryController {
    @Autowired
    private CategoryService categoryService;
    @PostMapping
    public R<String> save(@RequestBody Category category){

        categoryService.save(category);

        return R.success("添加成功");
}

@GetMapping("/page")
    public R<Page> showall(int page,int pageSize){
       log.info("分页查询,当前页数:{},分页大小:{})",page,pageSize);
        /*
        * 创建分页器
        * */

          Page p  = new Page(page,pageSize);

          /*
          * 条件构造器，排序
          * */
    LambdaQueryWrapper<Category> cr = new LambdaQueryWrapper();
           cr.orderByAsc(Category::getSort);
           categoryService.page(p,cr);
           return R.success(p);
}
@DeleteMapping
    public R delete(Long ids){

        categoryService.remove(ids);

        return R.success("删除成功");


}
    @PutMapping
    public R update(@RequestBody  Category category){

        categoryService.updateById(category);

        return  R.success("修改成功");
    }

@GetMapping("/list")
    public R<List<Category>> dishpage(Category category){
        log.info("查询菜品分类");
        if (category.getType() == null){
           return R.success(categoryService.list());

        }
        else {
            LambdaQueryWrapper<Category> lr = new LambdaQueryWrapper();
            lr.eq(Category::getType, category.getType());
            lr.orderByAsc(Category::getSort);
            List<Category> list = categoryService.list(lr);
            return R.success(list);
        }
}


}
