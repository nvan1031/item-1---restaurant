package com.rhx.Controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.rhx.Common.R;
import com.rhx.Service.UserService;
import com.rhx.Utils.ValidateCodeUtils;
import com.rhx.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author 任恒绪
 * @version 1.0
 */

@RestController
@Slf4j
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private RedisTemplate redisTemplate;
/*
* 发送验证码
* */
    @PostMapping("/sendMsg")
    public R getcode(@RequestBody User user, HttpSession session){
      //获取验证码
         String phone = user.getPhone();
//        判定手机号不为空
        if (StringUtils.hasText(phone)){

            //   生成验证码
            String code = ValidateCodeUtils.generateValidateCode4String(4);
       log.info("code = {}",code);
//        阿里云短信将验证码发送给用户

//test
/*//        保存验证码到Seesion
        session.setAttribute(phone,code);
        */
            //修改为保存到Redis
            redisTemplate.opsForValue().set(phone,code,5, TimeUnit.MINUTES);



        return R.success("短信发送成功");

        }
        return R.error("短信发送失败");


    }
    /*
    *
    * 登录
    *
    * */
    @PostMapping("/login")
    public R login(@RequestBody Map userwithcode,HttpSession session){
        //获取手机号和验证码
        String phone  =(String)  userwithcode.get("phone");
        String code  = (String) userwithcode.get("code");

        User newuser =  new User();
        newuser.setPhone(phone);

/*      获取Seesion中的验证码
        String validcode = (String) session.getAttribute(phone);
*/

//        从Redis中获取验证码
       String validcode = (String) redisTemplate.opsForValue().get(phone);


//      做对比验证.
        if (code.equals(validcode)){
//            判断是不是新用户，是的话保存注册
            LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(User::getPhone,phone);
            User u =  userService.getOne(queryWrapper);
            if (u == null){
                userService.save(newuser);
                session.setAttribute("user",newuser.getId());
//                登陆成功，删除Redis中的对应数据
                redisTemplate.delete(phone);

                return R.success(newuser);
            }
            else {
                session.setAttribute("user",u.getId());
                redisTemplate.delete(phone);
                return R.success(u);

            }



        }


           return R.error("登陆失败");


    }





}
