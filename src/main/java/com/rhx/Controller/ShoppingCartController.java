package com.rhx.Controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.rhx.Common.BaseContent;
import com.rhx.Common.R;
import com.rhx.Service.ShoppingCartService;
import com.rhx.domain.Dish;
import com.rhx.domain.ShoppingCart;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author 任恒绪
 * @version 1.0
 */
@RestController
@Slf4j
@RequestMapping("/shoppingCart")
public class ShoppingCartController {
    @Autowired
    ShoppingCartService shoppingCartService;

    @PostMapping("/add")
    public R add(@RequestBody ShoppingCart shoppingCart){

        log.info("购物车数据： {}",shoppingCart);

        //获取当前用户ID存入ShoppingCart
        Long id =   BaseContent.getId();
        shoppingCart.setUserId(id);

// 查询当前菜品是否在购物车中存在.符合两个条件1：同一用户2统一菜品或者套餐。
        LambdaQueryWrapper <ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
           //当添加的是菜品
        if (shoppingCart.getSetmealId() == null){
            queryWrapper.eq(ShoppingCart::getDishId,shoppingCart.getDishId());
        }
//        当添加的是套餐
        else {
            queryWrapper.eq(ShoppingCart::getUserId,shoppingCart.getDishId());
        }
         queryWrapper.eq(ShoppingCart::getUserId,shoppingCart.getUserId());
         ShoppingCart st = shoppingCartService.getOne(queryWrapper);
         //如果购物车中已经有数据了；将number+1并进行更新操作；
         if (st != null){
             st.setNumber((st.getNumber()+1));
            shoppingCartService.updateById(st);
            shoppingCart = st;
         }
//         购物车中无数据,
         else {
             shoppingCart.setNumber(1);
             shoppingCartService.save(shoppingCart);
         }
         shoppingCart.setCreateTime(LocalDateTime.now());
         return R.success(shoppingCart);

    }

    /*
    *
    * 查询当前用户的购物车
    * */
    @GetMapping("/list")
    public  R<List<ShoppingCart>> list(){
//       获取当前用户ID
       Long id =  BaseContent.getId();
        log.info("查询购物车，用户id= {}",id);

//根据用户ID查询购物车数据
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId,id);
        queryWrapper.orderByDesc(ShoppingCart::getCreateTime);
       List<ShoppingCart> list =   shoppingCartService.list(queryWrapper);
       return R.success(list);

    }
    /*
    *
    * 清空购物车
    * */
    @DeleteMapping("/clean")
    private  R clean(){



        //获取当前用户ID
   Long id = BaseContent.getId();
        log.info("清空购物车，用户id= {}",id);
   //删除当前用户的购物车数据
        LambdaQueryWrapper <ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId,id);
        shoppingCartService.remove(queryWrapper);
        return R.success("清空购物车成功");

    }





}
