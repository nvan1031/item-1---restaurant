package com.rhx.Controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rhx.Common.R;
import com.rhx.Service.CategoryService;
import com.rhx.Service.SetmealService;
import com.rhx.domain.Category;
import com.rhx.domain.Dto.SetmealDto;
import com.rhx.domain.Setmeal;
import com.rhx.domain.SetmealDish;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author 任恒绪
 * @version 1.0
 */
@RestController
@Slf4j
@RequestMapping("/setmeal")
public class MealController {
    @Autowired
    private SetmealService setmealServicevice;
    @Autowired
    private CategoryService categoryService;
   @Autowired
   private RedisTemplate redisTemplate;

    @PostMapping
    public R saveMealwithDish(@RequestBody SetmealDto setmealDto){
        //增加数据，清空相关套餐分类Redis信息。
        //构造key
        String key = "meal_"+setmealDto.getCategoryId()+"_1";
        // 删除key对应的数据
        redisTemplate.delete(key);

        setmealServicevice.saveMealDish(setmealDto);

        return R.success("增加套餐成功");

    }


     @GetMapping("/page")
    public R pages(int page,int pageSize,String name){
        //创建分页构造器
        Page<Setmeal> pageinfo= new Page(page,pageSize);

        Page<SetmealDto> Dtopageinfo= new Page();
         LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper();
         queryWrapper.like(name != null,Setmeal::getName,name);
         queryWrapper.orderByDesc(Setmeal::getUpdateTime);

         setmealServicevice.page(pageinfo,queryWrapper);

         //拷贝信息到新的pageinfo
         BeanUtils.copyProperties(pageinfo,Dtopageinfo,"records");
         List<Setmeal> records = pageinfo.getRecords();
         List<SetmealDto> list = records.stream().map((item->{
             SetmealDto setmealDto = new SetmealDto();
//            通过ID查询套餐分类信息
             Long id = item.getCategoryId();
            Category category =  categoryService.getById(id);
             String categoryname = category.getName();
             BeanUtils.copyProperties(item,setmealDto);
             setmealDto.setCategoryName(categoryname);
             return setmealDto;
         })).collect(Collectors.toList());
         Dtopageinfo.setRecords(list);

         return R.success(Dtopageinfo);


     }
     @DeleteMapping
    public R  deleteMealDish(@RequestParam List<Long> ids){
         //增加数据，清空Redis信息。
         //构造key
         Set keys  = redisTemplate.keys("meal_*");

         // 删除key对应的数据
         redisTemplate.delete(keys);
        setmealServicevice.deleteMealDish(ids);
        return  R.success("删除成功");


     }
     @GetMapping("/list")
    public  R <List<Setmeal>> list(Setmeal setmeal){
         //构造key
        String key = "meal_"+setmeal.getCategoryId()+"_1";
        //从Redis中查询是否存有数据
         List<Setmeal> setmealList = (List<Setmeal>) redisTemplate.opsForValue().get(key);
           if (setmealList != null)
               return R.success(setmealList);


         LambdaQueryWrapper <Setmeal> queryWrapper = new LambdaQueryWrapper<>();
         queryWrapper.eq(Setmeal::getCategoryId,setmeal.getCategoryId());
         queryWrapper.orderByDesc(Setmeal::getUpdateTime);

         setmealList =  setmealServicevice.list(queryWrapper);
         //将数据存储到Redis
         redisTemplate.opsForValue().set(key,setmealList);

         return  R.success(setmealList);


     }








}
