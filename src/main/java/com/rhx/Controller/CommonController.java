package com.rhx.Controller;

import com.rhx.Common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.UUID;

/**
 * @author 任恒绪
 * @version 1.0
 */
@RestController
@RequestMapping("/common")
@Slf4j
public class CommonController {
    //存储路径读取
    @Value("${panda.path}")
    private  String  path;
    @PostMapping("/upload")
    public R upload(MultipartFile file){

      //获取文件名后缀
      String originalFilename = file.getOriginalFilename();
      String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
      //利用UUID生成随机名字.
       String name =  UUID.randomUUID().toString()+suffix;
        log.info("文件流 : {}",file.toString());
       //判断当前路径是否存在
        File dir = new File(path);
        if(!dir.exists())
         dir.mkdirs();
        //存储文件到指定位置
        try {
            file.transferTo(new File(path+name));
        } catch (IOException e) {
            e.printStackTrace();
        }

        log.info(path+name);
        return R.success(name);
    }
@GetMapping("/download")
public void load(HttpServletResponse response,String name) {
log.info("读取到:"+name);

    try {

        //从路径中寻找对应name文件,用输出流读取
        FileInputStream fileInputStream = new FileInputStream(new File(path+name));
        //获取响应的输出流
        ServletOutputStream servletOutputStream = response.getOutputStream();
        response.setContentType("image/jpeg");
        //将其写进数组
        byte[] bytes = new byte[1024];
        log.info("数据流"+bytes.toString());
        int len = 0;
        while ((len = fileInputStream.read(bytes))!= -1){
           servletOutputStream.write(bytes,0,len);
           servletOutputStream.flush();
        }
        //设置返回响应类型

        //关闭流
        fileInputStream.close();
        servletOutputStream.close();

    } catch (Exception e) {
        e.printStackTrace();
    }


}



}
