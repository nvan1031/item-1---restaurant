package com.rhx.Controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rhx.Common.R;
import com.rhx.Service.CategoryService;
import com.rhx.Service.DishFlavorsService;
import com.rhx.Service.DishService;
import com.rhx.domain.Category;
import com.rhx.domain.Dish;
import com.rhx.domain.DishFlavor;
import com.rhx.domain.Dto.DishDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author 任恒绪
 * @version 1.0
 */
@RestController
@Slf4j
@RequestMapping("/dish")
public class DishController {
    @Autowired
   private DishService dishService;
   @Autowired
   private CategoryService categoryService;
   @Autowired
   private DishFlavorsService dishFlavorsService;
   @Autowired
   private RedisTemplate redisTemplate;

    /*
    *
    * 增加菜品
    *
    *
    * */
    @PostMapping
    public R save(@RequestBody  DishDto dishDto){
     log.info(dishDto.toString());
     dishService.savewithFlavors(dishDto);
     String key = "dish_"+dishDto.getCategoryId()+"_1";
 //        清除Redis中存储的当前分类的信息
     redisTemplate.delete(key);
        return R.success("添加菜品成功");

    }

    /*
    *分页查询
    * */
    @GetMapping("/page")
    public R page(int page,int pageSize,String name){
//        按页查询
        LambdaQueryWrapper <Dish> lr = new LambdaQueryWrapper();
//        查询条件建立
        lr.like(name != null,Dish::getName,name);
        lr.orderByDesc(Dish::getUpdateTime);
        Page<Dish> pageinfo = new Page(page,pageSize);


       dishService.page(pageinfo,lr);
//        建立新的返回数据
        Page <DishDto> newpageinfo = new Page();
        // 复制原先page里面除了recors外的数据;
        BeanUtils.copyProperties(pageinfo,newpageinfo,"records");
        //
        List<Dish> records = pageinfo.getRecords();
       List<DishDto> newrecords  =   records.stream().map(item->{
            DishDto dishDto = new DishDto();
//            根据当前item的id来查询对应name并且赋值
            Category category = categoryService.getById(item.getCategoryId());
            dishDto.setCategoryName(category.getName());
            BeanUtils.copyProperties(item,dishDto);
            return dishDto;
        }).collect(Collectors.toList());

//       将转变好的recods赋值给newpageinfo；
       newpageinfo.setRecords(newrecords) ;
       return R.success(newpageinfo);

    }

    @GetMapping("/{id}")
    public R get(@PathVariable Long id){

        log.info("修改信息"+id);
      DishDto dishDto =  dishService.getdishwithFlavors(id);
//        清除Redis中存储的当前分类的信息
      String key = "dish_"+dishDto.getCategoryId()+"_1";
        redisTemplate.delete(key);
       return  R.success(dishDto);

    }

    @PutMapping
    public R updata(@RequestBody  DishDto dishDto){

        dishService.updatadishwithFlavors(dishDto);


        return R.success("修改菜品成功");
    }
  @GetMapping("/list")
    public  R<List<DishDto>> getlist(Long categoryId){
      List<DishDto> newlist;
        log.info("当前套餐id"+categoryId);
//          构造key
      String key = "dish_"+categoryId+"_1";
//       通过key查询Redis中是否有数据
      newlist = (List<DishDto>)redisTemplate.opsForValue().get(key);
//      数据不为空则直接返回查询到的数据
      if(newlist != null)
          return R.success(newlist);

        LambdaQueryWrapper<Dish> lr = new LambdaQueryWrapper<>();
        lr.eq(Dish::getCategoryId,categoryId);
        lr.eq(Dish::getStatus,1);
        lr.orderByDesc(Dish::getSort).orderByAsc(Dish::getUpdateTime);
        List<Dish> list = dishService.list(lr);


        //创建新的Dishdto的列表，利用流将每个Dishdto都赋值
        newlist =  list.stream().map((item->{
            DishDto dishDto = new DishDto();
           // 利用Beanutils拷贝
            BeanUtils.copyProperties(item,dishDto);
            //获取当前菜品id
            Long id =  item.getId();
            //根据id查询口味，并赋值到新的Dishdto
            LambdaQueryWrapper <DishFlavor>queryWrapper = new LambdaQueryWrapper();
            queryWrapper.eq(DishFlavor::getDishId,id);
            List<DishFlavor> flavors = dishFlavorsService.list(queryWrapper);
            dishDto.setFlavors(flavors);
            //返回新的Dishdto
            return dishDto;

        })).collect(Collectors.toList());

//               将查询到的数据存入Redis
      redisTemplate.opsForValue().set(key,newlist,60, TimeUnit.MINUTES);
//               返回新的list
     return R.success(newlist);

  }

/*
* 删除菜品
*
* */
@DeleteMapping
    public R delete(Long ids){
      dishService.removeById(ids);
//        清除Redis中存储的所有的信息
    Set keys =redisTemplate.keys("dish_*") ;
    redisTemplate.delete(keys);
      return  R.success("删除菜品成功");

}
/*
* 停售或者起售菜品
*
* */
   @PostMapping("/status/{sts}")
    public R shutseal( @PathVariable int sts,@RequestParam List<Long> ids){
       int newstatus;
       //修改状态到另一状态

//       修稿ids中的状态为新状态

        dishService.updatestatusByids(sts,ids);
//        清除Redis中存储的所有的信息
       Set keys =redisTemplate.keys("dish_*") ;
       redisTemplate.delete(keys);
       return R.success("更新状态成功");


   }




}
