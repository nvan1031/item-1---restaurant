package com.rhx.Controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.rhx.Common.BaseContent;
import com.rhx.Common.R;
import com.rhx.Service.AddressBookService;
import com.rhx.Service.OrderDetailService;
import com.rhx.Service.OrderService;
import com.rhx.Service.ShoppingCartService;
import com.rhx.domain.AddressBook;
import com.rhx.domain.Orders;
import com.rhx.domain.ShoppingCart;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.pattern.PathPattern;

/**
 * @author 任恒绪
 * @version 1.0
 */
@RestController
@Slf4j
@RequestMapping("/order")
public class OrderController {

@Autowired
private OrderService orderService;
    /*
提交订单
 */

    @PostMapping("/submit")
    public R submit(@RequestBody Orders orders){
        orderService.submit(orders);

       return R.success("下单成功");
    }



}
