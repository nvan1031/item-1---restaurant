package com.rhx.Controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rhx.Common.R;
import com.rhx.Service.EmployeeService;
import com.rhx.domain.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 任恒绪
 * @version 1.0
 */

@RestController
@RequestMapping("/employee")
@Slf4j
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    /*
    *
    * 登录功能
    *
    * */
    @PostMapping("/login")
    public R login(HttpServletRequest request, @RequestBody Employee employee){
        String password = employee.getPassword();
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(Employee::getUsername,employee.getUsername());
        Employee res = employeeService.getOne(queryWrapper);
        if (res == null)
            return R.error("此用户不存在!!!");
        if (!res.getPassword().equals(password))
            return R.error("密码错误！请重新输入!");
        if (res.getStatus() == 0)
            return R.error("用户已禁用!!!");
        request.getSession().setAttribute("employee",res.getId());
        return R.success(res);

    }
    @PostMapping("/logout")
    public R logout(HttpServletRequest request){
        request.getSession().removeAttribute("employee");

        return R.success("退出成功");
    }
/*
* 增加员工
*
* */
    @PostMapping
    public R addeemployee(HttpServletRequest request,@RequestBody Employee employee){

//        employee.setCreateTime(LocalDateTime.now());
//
//        employee.setUpdateTime(LocalDateTime.now());

        long creatID = (long)request.getSession().getAttribute("employee");


//        employee.setCreateUser(creatID);
//
//        employee.setUpdateUser(creatID);
        //使用MD5加密的密码
        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));

        employeeService.save(employee);

        return  R.success("新增员工成功");

    }
    /*
    *分页查询
    *
    * */
    @GetMapping("/page")
    public R<Page> getlistEmployee(int page,int pageSize,String name){
         //创建分页构造器
        Page pageinfo = new Page(page,pageSize);
        //创造条件构造器
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.like(StringUtils.hasText(name),Employee::getName,name);
       //增加排序条件
        queryWrapper.orderByDesc(Employee::getUpdateTime);

        //执行查询
        employeeService.page(pageinfo,queryWrapper);

        //将分页信息返回
        return R.success(pageinfo);

    }
    @PutMapping
    public R<String> update(HttpServletRequest servletRequest,@RequestBody Employee employee){

//        employee.setUpdateTime(LocalDateTime.now());
//        employee.setUpdateUser((long)servletRequest.getSession().getAttribute("employee"));
        employeeService.updateById(employee);
        return R.success("修改员工信息成功");

    }
    @GetMapping("/{id}")
    public R<Employee> updatebyid( @PathVariable long id){
        log.info("信息回显");
        Employee employee = employeeService.getById(id);

        if (employee != null)
            return R.success(employee);
        else
            return  R.error("无此员工");
    }





}
